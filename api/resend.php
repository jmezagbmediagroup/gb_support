<?php
ini_set('display_errors', 0);

require '../libs/phpMailer/PHPMailerAutoload.php';

$entry_id = $_POST['entry_id'];
$to_email = $_POST['email'];

if(!isset($entry_id) || empty($entry_id)) {
    header('Content-Type: application/json');
    echo json_encode(['error' => true, 'msg' => 'No entry ID']);
    exit;
}

$DBHOST = 'localhost';
$DBNAME = 'gbmediag_support'; //ostickets
$DBUSER = 'gbmediag_admin'; //ostickets_user
$DBPASS = '4JGPsE3ehX-v'; //123456

$conn = mysqli_connect($DBHOST, $DBUSER, $DBPASS, $DBNAME);
mysqli_set_charset($conn, "utf8");

if (!$conn) {
    header('Content-Type: application/json');
    echo json_encode([
        'error' => true,
        'msg' => 'Could not connect to database. Please check credentials.',
        'error_code' => mysqli_connect_errno(),
        'error_msg' => mysqli_connect_error()
    ]);
    exit;
}

$sql = "SELECT
            entry.id,
            entry.body,
            ticket.ticket_id,
            ticket.number AS ticket_number,
            ticket_data.subject,
            entry.staff_id,
            CONCAT(staff.firstname, ' ', lastname) AS staff_name,
            staff.email AS staff_email,
            staff.signature,
            config.value AS support_name
        FROM
            ost_thread_entry entry 
            INNER JOIN ost_thread thread ON thread.id = entry.thread_id
            INNER JOIN ost_ticket ticket ON ticket.ticket_id = thread.object_id
            INNER JOIN ost_ticket__cdata ticket_data ON ticket_data.ticket_id = ticket.ticket_id
            LEFT JOIN ost_staff staff ON staff.staff_id = entry.staff_id
            LEFT JOIN ost_config config ON config.id = 3
        WHERE
            entry.id = $entry_id;";

$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_assoc($query);

$support_name = $result['support_name'];

$ticket_id = $result['ticket_id'];
$ticket_number = $result['ticket_number'];
$subject = $result['subject'];
$body = $result['body'];

$staff_id = $result['staff_id'];
$staff_name = $result['staff_name'];
$staff_email = $result['staff_email'];
$staff_signature = $result['signature'];

// -------------------------- SEND EMAIL  -------------------------- //

// Account details
$email_host = 'grupo-bedoya.com';
$email_username = 'soporte@grupo-bedoya.com';
$email_password = 'pVPZDoS06PeM8gwA';
$port = 465;
$SMTPSecure = 'ssl';

// Email Info
$mail_subject = "Re: $subject [#$ticket_number]";
$mail_body = $body;

if(!empty($staff_signature)) {
    $mail_body .= "<br><br>";
    $mail_body .= "$staff_signature";
}

$mail = new PHPMailer(); // create a new object

$mail->IsSMTP();
$mail->SMTPDebug = 0;
$mail->SMTPAuth = true;
$mail->SMTPSecure = $SMTPSecure;
$mail->Host = $email_host;
$mail->Port = $port;
$mail->Username = $email_username;
$mail->Password = $email_password;
$mail->SetFrom($email_username, $support_name);
$mail->IsHTML(true);
$mail->CharSet = 'UTF-8';
$mail->Subject = $mail_subject;
$mail->Body = $mail_body;
$mail->AddAddress($to_email);

if(!$mail->Send()) {
    header('Content-Type: application/json');
    echo json_encode(['error' => true, 'msg' => 'Could not send email.', 'error_msg' => $mail->ErrorInfo]);
} else {
    $sql = "INSERT INTO 
                config_resend_emails (to_email, ticket_id, ticket_number, ticket_entry_id, staff_id, date_send) 
            VALUES 
                ('$to_email', '$ticket_id', '$ticket_number', '$entry_id', '$staff_id', NOW());";

    $query = mysqli_query($conn, $sql);
    header('Content-Type: application/json');
    echo json_encode(['error' => false, 'msg' => 'Email has been sent.']);
}